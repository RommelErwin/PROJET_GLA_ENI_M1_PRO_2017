package fr.emse.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MoneyTest {
	Money m12CHF;
	Money m14CHF;
	Money expected;
	Money result;

	@Before
	public void initilize() {
		m12CHF = new Money(12, "CHF");
		m14CHF = new Money(14, "CHF");
		expected = new Money(26, "CHF");
		result = (Money)m12CHF.add(m14CHF);
	}
	
	/*
	//G�n�r� automatiquement
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	*/
	
    @Test
    public void testSimpleAdd() {
        //Money m12CHF = new Money(12, "CHF"); // cr�ation de donn�es
        //Money m14CHF = new Money(14, "CHF");
        //Money expected = new Money(26, "CHF");
        //Money result = m12CHF.add(m14CHF); // ex�cution de la m�thode test�e
        assertTrue(expected.equals(result)); // comparaison
    }
    
    @Test
    public void testEquals() {
        //Money m12CHF = new Money(12, "CHF");
        //Money m14CHF = new Money(14, "CHF");
        assertTrue(!m12CHF.equals(null));
        assertEquals(m12CHF, m12CHF);
        assertEquals(m12CHF, new Money(12, "CHF"));
        assertTrue(!m12CHF.equals(m14CHF));
    }
}
