package fr.emse.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(value = { MoneyBagTest.class, MoneyTest.class })

public class AllTests {
	//Tous les tests v�rifi�s
}
