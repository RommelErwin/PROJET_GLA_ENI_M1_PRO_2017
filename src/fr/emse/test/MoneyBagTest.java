package fr.emse.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MoneyBagTest {
	Money f12CHF;
	Money f14CHF;
	Money f7USD;
	Money f21USD;
	MoneyBag fMB1;
	MoneyBag fMB2;
	
	/*
	//G�n�r� automatiquement
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	*/
	
	@Before
    public void setUp() {
        f12CHF = new Money(12, "CHF");
        f14CHF = new Money(14, "CHF");
        f7USD = new Money(7, "USD");
        f21USD = new Money(21, "USD");
        fMB1 = new MoneyBag(f12CHF, f7USD);
        fMB2 = new MoneyBag(f14CHF, f21USD);
    }

    @Test
    public void testBagEquals() {
        assertTrue(!fMB1.equals(null));
        assertEquals(fMB1, fMB1);
        assertTrue(!fMB1.equals(f12CHF));
        assertTrue(!f12CHF.equals(fMB1));
        assertTrue(!fMB1.equals(fMB2));
    }
    
    @Test
	public void testMixedSimpleAdd() {
		// [12 CHF] + [7 USD] == {[12 CHF][7 USD]}
		Money bag[] = { f12CHF, f7USD };
		MoneyBag expected = new MoneyBag(bag);
		assertEquals(expected, f12CHF.add(f7USD));
	}
    
	@Test
	public void testBagSimpleAdd() {
		// {[26 CHF][21 USD]} = [14 CHF] + {[12 CHF][21 USD]} 
		Money bag[] = { f12CHF, f21USD };
		Money f26CHF = new Money(f12CHF.amount() + f14CHF.amount(), "CHF");
		MoneyBag mb1 = new MoneyBag(f26CHF, f21USD);
		MoneyBag mb = new MoneyBag(bag);
		MoneyBag mb2 = (MoneyBag)f14CHF.add(mb);
		assertEquals(mb1, mb2);
	}
	
	@Test
	public void testSimpleBagAdd() {
		// {[26 CHF][21 USD]} = {[12 CHF][21 USD]} + [14 CHF]
		Money bag[] = { f12CHF, f21USD };
		Money f26CHF = new Money(f12CHF.amount() + f14CHF.amount(), "CHF");
		MoneyBag mb1 = new MoneyBag(f26CHF, f21USD);
		MoneyBag mb = new MoneyBag(bag);
		MoneyBag mb2 = (MoneyBag)mb.add(f14CHF);
		assertEquals(mb1, mb2);
	}
	
	@Test
	public void testBagBagAdd() {
		// {[26 CHF][28 USD]} = {[12 CHF][21 USD]} + {[14 CHF][7 USD]}
		Money bag1[] = { f12CHF, f21USD };
		Money bag2[] = { f14CHF, f7USD };
		MoneyBag mbi = new MoneyBag(bag1);
		MoneyBag mbj = new MoneyBag(bag2);
		Money bag[] = { (Money)f12CHF.add(f14CHF), (Money)f21USD.add(f7USD) };
		MoneyBag mb1 = new MoneyBag(bag);
		MoneyBag mb2 = (MoneyBag)mbi.add(mbj);
		assertEquals(mb1, mb2);
	}
	
	@Test
	public void testSimplify() {
		// [7 USD] = {[12 CHF][7 USD]} + [-12 CHF]
		Money m[] = {f7USD, f12CHF};
		Money neg = new Money(-12, "CHF");
		MoneyBag init = new MoneyBag(m);
		IMoney im = init.add(neg);
		if (im instanceof Money) {
			assertEquals(f7USD, (Money)im);
		}
	}
}
