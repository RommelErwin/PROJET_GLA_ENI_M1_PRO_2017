package fr.emse.test;

public class Money implements IMoney {
	private int fAmount;
    private String fCurrency;

    public Money(int amount, String currency) {
        fAmount = amount;
        fCurrency = currency;
    }

    public int amount() {
        return fAmount;
    }

    public String currency() {
        return fCurrency;
    }

    
	@Override
	public boolean equals(Object m) {
		if (m != null && m instanceof Money) {
			if ((((Money)m).currency()).equals(this.fCurrency) && ((Money)m).amount() == this.fAmount) {
				return true;
			}
			else return false;
		}
		else return false;
	}

	@Override
	public IMoney add(IMoney m) {
		if (m instanceof Money && ((Money)m).currency().equals(currency()))
			return new Money(amount() + ((Money)m).amount(), currency());
		else if (m instanceof Money && !((Money)m).currency().equals(currency())) {
			return new MoneyBag(this, ((Money)m));
		}
		else if (m instanceof MoneyBag) {
			Money[] mb = new Money[((MoneyBag)m).getfMonies().size() + 1];
			for (int i = 0; i < mb.length - 1; i++) {
				mb[i] = ((MoneyBag)m).getfMonies().get(i);
			}
			mb[mb.length - 1] = this;
			MoneyBag mg = new MoneyBag(mb);
			int nb = 0;
			for (int i = 0; i < mg.getfMonies().size(); i++) {
				if (mg.getfMonies().get(i).amount() == 0) {
					mg.getfMonies().remove(i);
				}
				else nb++;
			}
			
			if (nb == 0 || nb > 1) {
				return mg;
			}
			else if (nb == 1) {
				return new Money(mg.getfMonies().get(0).amount(), mg.getfMonies().get(0).currency());
			}
			else return null;
		}
		else return null;
	}
}
