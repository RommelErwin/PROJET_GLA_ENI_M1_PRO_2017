package fr.emse.test;

import java.util.Vector;

public class MoneyBag implements IMoney{
	private Vector<Money> fMonies = new Vector<Money>();

    MoneyBag(Money m1, Money m2) {
        appendMoney(m1);
        appendMoney(m2);
    }

    MoneyBag(Money bag[]) {
        for (int i = 0; i < bag.length; i++) appendMoney(bag[i]);
    }

    private void appendMoney(Money m) {
        if (fMonies.isEmpty()) {
            fMonies.add(m);
        } else {
            int i = 0;
            while ((i < fMonies.size()) && (!(fMonies.get(i).currency().equals(m.currency())))) i++;
            if (i >= fMonies.size()) {
                fMonies.add(m);
            } else {
                fMonies.set(i, new Money(fMonies.get(i).amount() + m.amount(),
                        m.currency()));
            }
        }
    }
    
    @Override
	public boolean equals(Object mb) {
		if (mb != null && mb instanceof MoneyBag) {
			if (this.fMonies.size() == (((MoneyBag)mb).fMonies).size()) {
				int test = 0;
				for (int i = 0; i < this.fMonies.size(); i++) {
					if (!(this.fMonies.get(i).equals(((MoneyBag)mb).fMonies.get(i)))) {
						test = 1;
						break;
					}
				}
				if (test == 0) {
					return true;
				}
				else return false;
			}
			else return false;
		}
		else return false;
	}

	@Override
	public IMoney add(IMoney m) {
		if (m instanceof Money) {
			appendMoney((Money)m);
			int nb = 0;
			for (int i = 0; i < fMonies.size(); i++) {
				if (fMonies.get(i).amount() == 0) {
					fMonies.remove(i);
				}
				else nb++;
			}
			
			if (nb == 0 || nb > 1) {
				return this;
			}
			else if (nb == 1) {
				return new Money(this.fMonies.get(0).amount(), this.fMonies.get(0).currency());
			}
			else return null;
		}
		else if (m instanceof MoneyBag) {
			for (int i = 0; i < ((MoneyBag)m).fMonies.size(); i++) {
				appendMoney(((MoneyBag)m).fMonies.get(i));
			}
			int nb = 0;
			for (int i = 0; i < fMonies.size(); i++) {
				if (fMonies.get(i).amount() == 0) {
					fMonies.remove(i);
				}
				else nb++;
			}
			
			if (nb == 0 || nb > 1) {
				return this;
			}
			else if (nb == 1) {
				return new Money(this.fMonies.get(0).amount(), this.fMonies.get(0).currency());
			}
			else return null;
		}
		else return null;
	}
	
	public Vector<Money> getfMonies() {
		return fMonies;
	}
	public void setfMonies(Vector<Money> fMonies) {
		this.fMonies = fMonies;
	}
}
